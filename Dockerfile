FROM quay.io/keboola/docker-custom-python:latest
ENV PYTHONIOENCODING utf-8

# Prepare the container
WORKDIR /home

COPY . /code/ 

# Execution
CMD python3 /code/main.py
