### What is this component for? ###

Keboola Connection component to create a custom color palette in GoodData platform. App has an option to add default color set from GD. More information: [GoodData Help Page](https://help.gooddata.com/display/doc/Importing+Custom+Color+Palettes)

### Configuration ###

You will need to know this info:

* GOODDATA PROJECT ID
* KBC STORAGE TOKEN
* GOODDATA WRITER ID

### Contact ###

* Email: vfisa.stuff at gmail.com  
* Twitter: VFisa