### Component Configuration ###

App to create a custom color palette in GoodData platform.  

#### GOODDATA PROJECT ID ####
Project ID can be identified using the URL of any page within a project:  
*https://secure.gooddata.com/#s=/gdc/projects/<PROJECT_ID>|objectPage|/...*

#### KBC STORAGE TOKEN ####
[How to obtain storage token](https://help.keboola.com/storage/tokens/)

#### GOODDATA WRITER ID ####
Writer configuration ID can be identified from the URL of the writer page:
*https://connection.keboola.com/admin/projects/kbcproject/writers/gooddata-writer/<WRITER_ID>*


#### Default Colors  ####
Select if you want to include a default GoodData color palette (YES/NO).  

#### Custom colors ####
Add as many custom colors as you want.

