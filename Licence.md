# Terms and Conditions

The Keboola GoodData Custom Colors App is built and offered by Martin Fiser (Fisa) as a third party component. It is provided as-is, without guarantees and support, and for no additional charge. 
You are free to use it or check out the [source code](https://bitbucket.org/VFisa/kbc_gooddata_custom_colors_component). Please contribute to the code and report any encountered bugs.
Component's task is to help user to generate configuration JSON for GoodData API so the custom colors can be user. 
API call is process by using GoodData Writer component authentication, so no sensitive information is being sent non-standard way, maintaining all Keboola recommended security standards along the way.

## Contact

Fisa    
Martin Fiser  
Vancouver, Canada (PST time)  
email: VFisa.stuff@gmail.com    